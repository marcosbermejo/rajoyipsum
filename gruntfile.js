module.exports = function(grunt) {

  // Project configuration.
    grunt.initConfig({
        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: ['js/modernizr-3.5.0.min.js', 'js/jquery-1.9.1.min.js', 'js/main.min.js'],
                dest: 'js/page.min.js',
            },
        },        
        uglify: {
            my_target: {
                files: {
                    'js/main.min.js': ['js/main.js']
                }
            }
        },
        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                'css/main.min.css': ['css/main.css']
                }
            }
        } , 
        htmlmin: {                                     // Task
            dist: {                                      // Target
                options: {                                 // Target options
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {                                   // Dictionary of files
                    'index.html': 'src/index.html',     // 'destination': 'source'
                }
            },
        },
        watch: {
            files: ['css/main.css', 'js/main.js', 'src/index.html'],
            tasks: ['default'],
        },
    });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat'); 
  grunt.loadNpmTasks('grunt-contrib-htmlmin'); 
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['uglify', 'concat', 'cssmin', 'htmlmin']);

};