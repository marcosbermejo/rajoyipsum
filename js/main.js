(function(){

    var chiquitoArray = [
        "cuanto mejor peor para todos",
        "y cuanto peor para todos mejor",
        "mejor para mí el suyo, beneficio político",
        "es el vecino el que elije al alcalde",
        "y es el alcalde el que quiere que sean",
        "los vecinos el alcalde",
        "somos sentimientos y tenemos seres humanos",
        "una cosa es ser solidario y otra es serlo a cambio de nada",
        "los españoles son muy españoles",
        "muy españoles y mucho españoles",
        "muy españoles",
        "mucho españoles",
        "y mucho españoles",
        "esto no es como el agua que cae del cielo sin que se sepa exactamente por qué",
        "los catalanes hacen cosas",
        "it's very difficult todo esto",
        "lo que no van a hacer nunca las máquinas es fabricar máquinas",
        "¿Ustedes piensan antes de hablar o hablan tras pensar?",
        "un vaso es un vaso y un plato es un plato",
        "viva el vino",
        "no es cosa menor, dicho de otra manera, es cosa mayor",
        "fin de la cita",
        "¿Y la europea?",
        "los chuches",
        "lo he escrito aquí y no entiendo mi letra",
        "es usted Ruiz, ruin, mezquino y miserable",
        "por las carreteras tienen que ir coches y de los aeropuertos tienen que salir aviones",
        "lo más importante que se puede hacer por vosotros es lo que vosotros podáis hacer por nosotros",
        "como decía Galileo, el movimiento siempre se acelera cuando se va detener",
        "A veces moverse es bueno, otras veces no",
        "a veces es mejor estar quieto y en otras no",
        "y en ocasiones es mejor estar en movimiento",
        "ETA es una gran nación",
        "no fue cosa menor",
        "grave afrenta a la estabilidad de",
        "ese señor del que usted me habla",
        "salvo algunas cosas",
        "como ustedes comprenderán",
        "exportar es positivo porque vendes lo que produces",
        "luego ya veremos",
        "¿eh?"
    ]

        var latinArray = ["sit amet", "consectetur", "adipisicing", "elit", "sed", "eiusmod", "tempor", "incididunt", "ut", "labore", "et", "dolore", "magna", "aliqua", "enim", "ad", "minim", "veniam", "quis", "nostrud", "exercitation", "ullamco", "laboris", "nisi", "ut", "aliquip", "ex", "commodo", "consequat", "duis", "aute", "irure", "dolor", "reprehenderit", "voluptate", "velit", "esse", "cillum","occaecat", "qui", "officia"];

        var paragraphNumber = 2;
        var firstLine = true;
        var buttonCopyDisable = true;



        $("#generar").click(function(){
            firstLine = true;
            
            $("#result").html(generateFullText(chiquitoArray));
        });


        function generateRandom(initialValue,endValue){
            /*Generate a random number between initialValue and endValue*/
            var randomInterval;
            var randomValue;
            if(endValue>=initialValue){
                randomInterval=endValue-initialValue;
                randomValue=Math.floor(Math.random()*randomInterval)+initialValue;
            }else{
                randomInterval=initialValue-endValue;
                randomValue=Math.floor(Math.random()*randomInterval)+endValue;
            }
            return randomValue;
        }

        function generateLine(wordsArray){ 
            /*generate a line of random words*/
            var oneLine = new String();
            var wordRandom = new String();
            var wordsNumber = generateRandom(4,12);
            var wordRandomIndex;
            var lastWordExclamation = false;
            var i;
            
            wordRandomIndex = generateRandom(0,wordsArray.length);
            
            if(firstLine){
                oneLine="Lorem Rajoy Ipsum";
                firstLine=false;
            }else{
                oneLine=wordsArray[wordRandomIndex];
            }
            
            for (i=1;i<wordsNumber;i++){
                wordRandomIndex = generateRandom(0,wordsArray.length);
                wordRandom=wordsArray[wordRandomIndex];
                
                /*If the last word has an exclamation, it set the following letter to capital*/
                if(lastWordExclamation==true){ 
                    wordRandom=wordRandom[0].toUpperCase()+wordRandom.substring(1);
                    lastWordExclamation = false;
                }
                
                if(wordRandom.charAt(wordRandom.length-1)=='!'){
                    lastWordExclamation = true;
                }else{
                    lastWordExclamation = false;
                }
                
                oneLine=oneLine + ' ' + wordRandom;   
            }
            
            oneLine=oneLine[0].toUpperCase()+oneLine.substring(1);
            
            /*Add a period to the end of the line, unless the last character is an exclamation mark*/
            if(oneLine.charAt(oneLine.length-1)=='!'){
                oneLine=oneLine+' ';
            }else{
                oneLine=oneLine+'. ';
            }
            
            return oneLine;
        }

        function generateParagraph(wordsArray){
            /*Generate a paragraph with lines*/
            var oneParagraph = new String();
            var linesNumber = generateRandom(5,10);
            var i;
            
            oneParagraph=generateLine(wordsArray);
            
            for (i=1;i<linesNumber;i++){
                oneParagraph=oneParagraph + generateLine(wordsArray);   
            }
            
            /*Wrap the paragraph inside <p> tags*/
            oneParagraph='<p>'+oneParagraph+'</p>';
            
            return oneParagraph;
        }

        function generateFullText(wordsArray){
            /*Put together the number of paragraphs set by the user*/
            var FullText = new String();
            var i;
            
            FullText=generateParagraph(wordsArray);   

            for (i=1;i<paragraphNumber;i++){
                FullText=FullText+generateParagraph(wordsArray);  
            }
            return FullText;
        }






})()